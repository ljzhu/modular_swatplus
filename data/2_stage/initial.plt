initial.plt: Plant community initialization - 2-stage ditch
NAME   PLANTS_COM             CPNM       IGRO      LAI	   BIOMS   PHUACC      POP     YRMAT     RSDIN   
frst            1     
                              frst          1     0.00      0.00     0.00     0.00      0.00  10000.00
past            1
                              past          0     0.00      0.00     0.00     0.00      0.00   3000.00  							  
agrl            1        
                              agrl          0     0.00      0.00     0.00     0.00      0.00   1000.00
watr            1
                              watr          0     0.00      0.00     0.00     0.00      0.00      0.00
utrn            1  
                              berm          0     0.00      0.00     0.00     0.00      0.00   3000.00
csoy            2    
                              corn          0     0.00      0.00     0.00     0.00      0.00   2000.00       
                              soyb          0     0.00      0.00     0.00     0.00      0.00   2000.00 
ryeg            1     
                              ryeg          1     1.00    500.00     0.00     0.00      0.00   2000.00
cana            1     
                              cana          1     1.00    500.00     0.00     0.00      0.00   2000.00