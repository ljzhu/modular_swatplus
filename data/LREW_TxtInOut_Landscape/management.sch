management.sch: Management schedules - LREW Sub Water
    MGT_NAME    NUMB_OPS   NUMB_AUTO OP_TYPE   TRIG_TYPE         DAY     HU_SCH COND      OP_DATA1 OP_DATA2 OP_OVERRIDE
      mgt001          33           0                                                  
                        till           4           1      0.000       riprsubs     null       0.000
                        till           4          15      0.000       riprsubs     null       0.000
                        till           5           1      0.000        fldcult     null       0.000
                        plnt           5           2      0.000           pnut     null       0.000
						harv          10           1      0.000        peanuts     pnut       0.000
						kill          10           1      0.000           pnut     null       0.000
						till           3           1      0.000       riprsubs     null       0.000
						till           3          15      0.000       riprsubs     null       0.000
						till           4           1      0.000        fldcult     null       0.000
						plnt           4           2      0.000           corn     null       0.000
						fert           4           3      0.000        fert001     null       0.000
						fert           4           3      0.000        fert002     null       0.000
						harv           9           1      0.000          grain     corn       0.000
						kill           9           1      0.000           corn     null       0.000
						till           3          15      0.000       riprsubs     null       0.000
						till           4           1      0.000       riprsubs     null       0.000
						till           4          15      0.000       riprsubs     null       0.000
						till           5           1      0.000        fldcult     null       0.000
						plnt           5           2      0.000           cots     null       0.000
						fert           5           3      0.000        fert003     null       0.000
						fert           5           3      0.000        fert004     null       0.000
						harv          11           1      0.000          grain     cots       0.000
						kill          11           1      0.000           cots     null       0.000
						till           3          15      0.000       riprsubs     null       0.000
						till           4           1      0.000       riprsubs     null       0.000
						till           4          15      0.000       riprsubs     null       0.000
						till           5           1      0.000        fldcult     null       0.000
						plnt           5           2      0.000           cots     null       0.000
						fert           5           3      0.000        fert003     null       0.000
						fert           5           3      0.000        fert004     null       0.000
						harv          11           1      0.000          grain     cots       0.000
						kill          11           1      0.000           cots     null       0.000
						skip           0           0      0.000           null     null       0.000
      mgt002           3           0                                                   
                        plnt           3           1      0.000           past     null       0.000
                        harv          11           1      0.000    hay_cut_low     past       0.000
                        skip           0           0      0.000           null     null       0.000
      mgt003           0           0
      mgt004           0           0
      mgt005           0           0
      mgt006           0           0
      mgt007           3           0
                        plnt           3           1      0.000           berm     null       0.000
                        harv          11           1      0.000      grass_bag     berm       0.000
                        skip           0           0      0.000           null     null       0.000