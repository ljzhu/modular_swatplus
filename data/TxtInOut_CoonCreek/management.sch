management.sch: Management schedules - LREW Sub Water
    MGT_NAME    NUMB_OPS   NUMB_AUTO OP_TYPE   TRIG_TYPE         DAY     HU_SCH OP_DATA1 	OP_DATA2 OP_OVERRIDE
      c-sb-af-at-ai	   0           4	
	                       plant_harv_c-sb	
						   spring_fert	
						   minimum_till	
						   autoirr_str.8	      
      mgt001          33           0                                                      
                    till           4           1      0.000 riprsubs    	null       	0.000
                    till           4          15      0.000 riprsubs     	null       	0.000
                    till           5           1      0.000 fldcult     	null       	0.000
                    plnt           5           2      0.000 pnut     	null	  	0.000
					harv          10           1      0.000 pnut  		peanuts       	0.000
					kill          10           1      0.000 pnut     	null       	0.000
					till           3           1      0.000 riprsubs     	null       	0.000
					till           3          15      0.000 riprsubs     	null       	0.000
					till           4           1      0.000 fldcult     	null       	0.000
					plnt           4           2      0.000 corn     	null       	0.000
					fert           4           3      0.000 elem-n  	broadcast   	150.000
					fert           4           3      0.000 elem-p  	broadcast    	20.000
					harv           9           1      0.000 corn    	grain       	0.000
					kill           9           1      0.000 corn     	null       	0.000
					till           3          15      0.000 riprsubs     	null       	0.000
					till           4           1      0.000 riprsubs     	null       	0.000
					till           4          15      0.000 riprsubs     	null       	0.000
					till           5           1      0.000 fldcult     	null       	0.000
					plnt           5           2      0.000 cots     	null       	0.000
					fert           5           3      0.000 anh-nh3   	inject     	160.000
					fert           5           3      0.000 elem-p  	broadcast    	20.000
					harv          11           1      0.000 cots  		cotton_strip  	0.000
					kill          11           1      0.000 cots     	null       	0.000
					till           3          15      0.000 riprsubs     	null       	0.000
					till           4           1      0.000 riprsubs     	null       	0.000
					till           4          15      0.000 riprsubs     	null       	0.000
					pest           5           1      0.000 aatrex     	foliar    	0.000
					plnt           5           2      0.000 cots		null       	0.000
					fert           5           3      0.000 anh-nh3   	inject     	160.000
					fert           5           3      0.000 elem-p  	broadcast    	20.000
					harv          11           1      0.000 cots  		cotton_strip  	0.000
					kill          11           1      0.000 cots     	null       	0.000
					skip           0           0      0.000 null     	null       	0.000
      mgt002           3           0                                                   	
                                        plnt           3           1      0.000 past     	null       	0.000
                                        harv          11           1      0.000 past		hay_cut_low     0.000
                                        skip           0           0      0.000 null     	null       	0.000
      mgt003           0           0
      mgt004           0           0
      mgt005           0           0
      mgt006           0           0
      mgt007           3           0
                                        plnt           3           1      0.000 berm     	null       	0.000
                                        harv          11           1      0.000 berm		grass_bag     	0.000
                                        skip           0           0      0.000 null     	null       	0.000
      mgt008          33           0                                                      
					till           3           1      0.000 riprsubs     	null       	0.000
					till           3          15      0.000 riprsubs     	null       	0.000
					till           4           1      0.000 fldcult     	null       	0.000
					plnt           4           2      0.000 corn     	null       	0.000
					fert           4           3      0.000 elem-n  	broadcast   	150.000
					fert           4           3      0.000 elem-p  	broadcast    	20.000
					harv           9           1      0.000 corn		grain       	0.000
					kill           9           1      0.000 corn     	null       	0.000
					till           3          15      0.000 riprsubs     	null       	0.000
					till           4           1      0.000 riprsubs     	null       	0.000
					till           4          15      0.000 riprsubs     	null       	0.000
					till           5           1      0.000 fldcult     	null       	0.000
					plnt           5           2      0.000 cots     	null       	0.000
					fert           4           3      0.000 elem-n  	broadcast   	150.000
					fert           4           3      0.000 elem-p  	broadcast    	20.000
					harv          11           1      0.000 cots     	cotton_strip  	0.000
					kill          11           1      0.000 cots     	null       	0.000
					till           3          15      0.000 riprsubs     	null       	0.000
					till           4           1      0.000 riprsubs     	null       	0.000
					till           4          15      0.000 riprsubs     	null       	0.000
					till           5           1      0.000 fldcult     	null       	0.000
					plnt           5           2      0.000 cots     	null       	0.000
					fert           5           3      0.000 elem-n  	broadcast   	150.000
					fert           5           3      0.000 elem-p  	broadcast    	20.000
					harv          11           1      0.000 cots     	cotton_strip  	0.000
					kill          11           1      0.000 cots     	null       	0.000
                                        till           4           1      0.000 riprsubs     	null       	0.000
                                        till           4          15      0.000 riprsubs     	null       	0.000
                                        till           5           1      0.000 fldcult     	null       	0.000
                                        plnt           5           2      0.000 pnut     	null       	0.000
					harv          10           1      0.000 pnut     	peanuts     	0.000
					kill          10           1      0.000 pnut     	null       	0.000
					skip           0           0      0.000 null     	null       	0.000
      mgt009          33           0                                                      
					till           3          15      0.000 riprsubs     	null       	0.000
					till           4           1      0.000 riprsubs     	null       	0.000
					till           4          15      0.000 riprsubs     	null       	0.000
					till           5           1      0.000 fldcult     	null       	0.000
					plnt           5           2      0.000 cots     	null       	0.000
					fert           5           3      0.000 elem-n  	broadcast   	150.000
					fert           5           3      0.000 elem-p  	broadcast    	20.000
					harv          11           1      0.000 cots     	cotton_strip  	0.000
					kill          11           1      0.000 cots     	null       	0.000
					till           3          15      0.000 riprsubs     	null       	0.000
					till           4           1      0.000 riprsubs     	null       	0.000
					till           4          15      0.000 riprsubs     	null       	0.000
					till           5           1      0.000 fldcult     	null       	0.000
					plnt           5           2      0.000 cots     	null       	0.000
					fert           5           3      0.000 elem-n  	broadcast   	150.000
					fert           5           3      0.000 elem-p  	broadcast    	20.000
					harv          11           1      0.000 cots     	cotton_strip  	0.000
					kill          11           1      0.000 cots     	null       	0.000
                                        till           4           1      0.000 riprsubs     	null       	0.000
                                        till           4          15      0.000 riprsubs     	null       	0.000
                                        till           5           1      0.000 fldcult     	null       	0.000
                                        plnt           5           2      0.000 pnut     	null       	0.000
					harv          10           1      0.000 pnut     	peanuts     	0.000
					kill          10           1      0.000 pnut     	null       	0.000
					till           3           1      0.000 riprsubs     	null       	0.000
					till           3          15      0.000 riprsubs     	null       	0.000
					till           4           1      0.000 fldcult     	null       	0.000
					plnt           4           2      0.000 corn     	null       	0.000
					fert           4           3      0.000 elem-n  	broadcast   	150.000
					fert           4           3      0.000 elem-p  	broadcast    	20.000
					harv           9           1      0.000 corn     	grain     	.000
					kill           9           1      0.000 corn     	null       	0.000
					skip           0           0      0.000 null     	null       	0.000
      mgt010          33           0                                                      
					till           3          15      0.000 riprsubs     	null       	0.000
					till           4           1      0.000 riprsubs     	null       	0.000
					till           4          15      0.000 riprsubs     	null       	0.000
					till           5           1      0.000 fldcult     	null       	0.000
					plnt           5           2      0.000 cots     	null       	0.000
					fert           5           3      0.000 elem-n  	broadcast   	150.000
					fert           5           3      0.000 elem-p  	broadcast    	20.000
					harv          11           1      0.000 cots     	cotton_strip  	0.000
					kill          11           1      0.000 cots     	null       	0.000
                                        till           4           1      0.000 riprsubs     	null       	0.000
                                        till           4          15      0.000 riprsubs     	null       	0.000
                                        till           5           1      0.000 fldcult     	null       	0.000
                                        plnt           5           2      0.000 pnut     	null       	0.000
					harv          10           1      0.000 pnut     	peanuts     	0.000
					kill          10           1      0.000 pnut     	null       	0.000
					till           3           1      0.000 riprsubs     	null       	0.000
					till           3          15      0.000 riprsubs     	null       	0.000
					till           4           1      0.000 fldcult     	null       	0.000
					plnt           4           2      0.000 corn     	null       	0.000
					fert           4           3      0.000 elem-n  	broadcast   	150.000
					fert           4           3      0.000 elem-p  	broadcast    	20.000
					harv           9           1      0.000 corn		grain     	0.000
					kill           9           1      0.000 corn     	null       	0.000
					till           3          15      0.000 riprsubs     	null       	0.000
					till           4           1      0.000 riprsubs     	null       	0.000
					till           4          15      0.000 riprsubs     	null       	0.000
					till           5           1      0.000 fldcult     	null       	0.000
					plnt           5           2      0.000 cots     	null       	0.000
					fert           5           3      0.000 elem-n  	broadcast   	150.000
					fert           5           3      0.000 elem-p  	broadcast    	20.000
					harv          11           1      0.000 cots     	cotton_strip  	0.000
					kill          11           1      0.000 cots     	null       	0.000
					skip           0           0      0.000 null     	null       	0.000