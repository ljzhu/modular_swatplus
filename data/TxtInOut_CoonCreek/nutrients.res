nutrients.res: Reservoir nutrient inputs - SB_LS1
RES_NUT_NAME     MID_BEG     MID_END  MID_N_STL      N_STL  MID_P_STL      P_STL    CHLA_CO   SECCI_CO  THETA_N  THETA_P  C_NMIN  C_PMIN
res_nut001             4          10      0.500      2.000      1.000      0.500      1.000      1.000     1.      1.       .1      .01
