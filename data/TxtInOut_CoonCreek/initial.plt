initial.plt: Plant community initialization - SB_LS1
   PCOM_NAME     PLT_CNT    PLT_NAME        IGRO       LAI_INI     BM_INI PHU_ACC_INI        POP    YRS_INI    RSD_INI
     pcom001           1
                                frsd           1         0.000      0.000       0.000      0.000      0.000  10000.000
     pcom002           3
                                corn           0         0.000      0.000       0.000      0.000      0.000   1000.000
				                oats           0         0.000      0.000       0.000      0.000      0.000   1000.000
				                wpas           0         0.000      0.000       0.000      0.000      0.000   1000.000
